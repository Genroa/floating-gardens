"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.FloatingGardensEngine = void 0;
var _element_helpers = require("../helpers/element_helpers");
var _events = require("./events");
var _actions = require("./actions");
var _biomes = require("../biomes");
function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }
function _createForOfIteratorHelper(o, allowArrayLike) { var it = typeof Symbol !== "undefined" && o[Symbol.iterator] || o["@@iterator"]; if (!it) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = it.call(o); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }
function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }
function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i]; return arr2; }
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }
function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }
function _regeneratorRuntime() { "use strict"; /*! regenerator-runtime -- Copyright (c) 2014-present, Facebook, Inc. -- license (MIT): https://github.com/facebook/regenerator/blob/main/LICENSE */ _regeneratorRuntime = function _regeneratorRuntime() { return exports; }; var exports = {}, Op = Object.prototype, hasOwn = Op.hasOwnProperty, defineProperty = Object.defineProperty || function (obj, key, desc) { obj[key] = desc.value; }, $Symbol = "function" == typeof Symbol ? Symbol : {}, iteratorSymbol = $Symbol.iterator || "@@iterator", asyncIteratorSymbol = $Symbol.asyncIterator || "@@asyncIterator", toStringTagSymbol = $Symbol.toStringTag || "@@toStringTag"; function define(obj, key, value) { return Object.defineProperty(obj, key, { value: value, enumerable: !0, configurable: !0, writable: !0 }), obj[key]; } try { define({}, ""); } catch (err) { define = function define(obj, key, value) { return obj[key] = value; }; } function wrap(innerFn, outerFn, self, tryLocsList) { var protoGenerator = outerFn && outerFn.prototype instanceof Generator ? outerFn : Generator, generator = Object.create(protoGenerator.prototype), context = new Context(tryLocsList || []); return defineProperty(generator, "_invoke", { value: makeInvokeMethod(innerFn, self, context) }), generator; } function tryCatch(fn, obj, arg) { try { return { type: "normal", arg: fn.call(obj, arg) }; } catch (err) { return { type: "throw", arg: err }; } } exports.wrap = wrap; var ContinueSentinel = {}; function Generator() {} function GeneratorFunction() {} function GeneratorFunctionPrototype() {} var IteratorPrototype = {}; define(IteratorPrototype, iteratorSymbol, function () { return this; }); var getProto = Object.getPrototypeOf, NativeIteratorPrototype = getProto && getProto(getProto(values([]))); NativeIteratorPrototype && NativeIteratorPrototype !== Op && hasOwn.call(NativeIteratorPrototype, iteratorSymbol) && (IteratorPrototype = NativeIteratorPrototype); var Gp = GeneratorFunctionPrototype.prototype = Generator.prototype = Object.create(IteratorPrototype); function defineIteratorMethods(prototype) { ["next", "throw", "return"].forEach(function (method) { define(prototype, method, function (arg) { return this._invoke(method, arg); }); }); } function AsyncIterator(generator, PromiseImpl) { function invoke(method, arg, resolve, reject) { var record = tryCatch(generator[method], generator, arg); if ("throw" !== record.type) { var result = record.arg, value = result.value; return value && "object" == _typeof(value) && hasOwn.call(value, "__await") ? PromiseImpl.resolve(value.__await).then(function (value) { invoke("next", value, resolve, reject); }, function (err) { invoke("throw", err, resolve, reject); }) : PromiseImpl.resolve(value).then(function (unwrapped) { result.value = unwrapped, resolve(result); }, function (error) { return invoke("throw", error, resolve, reject); }); } reject(record.arg); } var previousPromise; defineProperty(this, "_invoke", { value: function value(method, arg) { function callInvokeWithMethodAndArg() { return new PromiseImpl(function (resolve, reject) { invoke(method, arg, resolve, reject); }); } return previousPromise = previousPromise ? previousPromise.then(callInvokeWithMethodAndArg, callInvokeWithMethodAndArg) : callInvokeWithMethodAndArg(); } }); } function makeInvokeMethod(innerFn, self, context) { var state = "suspendedStart"; return function (method, arg) { if ("executing" === state) throw new Error("Generator is already running"); if ("completed" === state) { if ("throw" === method) throw arg; return doneResult(); } for (context.method = method, context.arg = arg;;) { var delegate = context.delegate; if (delegate) { var delegateResult = maybeInvokeDelegate(delegate, context); if (delegateResult) { if (delegateResult === ContinueSentinel) continue; return delegateResult; } } if ("next" === context.method) context.sent = context._sent = context.arg;else if ("throw" === context.method) { if ("suspendedStart" === state) throw state = "completed", context.arg; context.dispatchException(context.arg); } else "return" === context.method && context.abrupt("return", context.arg); state = "executing"; var record = tryCatch(innerFn, self, context); if ("normal" === record.type) { if (state = context.done ? "completed" : "suspendedYield", record.arg === ContinueSentinel) continue; return { value: record.arg, done: context.done }; } "throw" === record.type && (state = "completed", context.method = "throw", context.arg = record.arg); } }; } function maybeInvokeDelegate(delegate, context) { var methodName = context.method, method = delegate.iterator[methodName]; if (undefined === method) return context.delegate = null, "throw" === methodName && delegate.iterator["return"] && (context.method = "return", context.arg = undefined, maybeInvokeDelegate(delegate, context), "throw" === context.method) || "return" !== methodName && (context.method = "throw", context.arg = new TypeError("The iterator does not provide a '" + methodName + "' method")), ContinueSentinel; var record = tryCatch(method, delegate.iterator, context.arg); if ("throw" === record.type) return context.method = "throw", context.arg = record.arg, context.delegate = null, ContinueSentinel; var info = record.arg; return info ? info.done ? (context[delegate.resultName] = info.value, context.next = delegate.nextLoc, "return" !== context.method && (context.method = "next", context.arg = undefined), context.delegate = null, ContinueSentinel) : info : (context.method = "throw", context.arg = new TypeError("iterator result is not an object"), context.delegate = null, ContinueSentinel); } function pushTryEntry(locs) { var entry = { tryLoc: locs[0] }; 1 in locs && (entry.catchLoc = locs[1]), 2 in locs && (entry.finallyLoc = locs[2], entry.afterLoc = locs[3]), this.tryEntries.push(entry); } function resetTryEntry(entry) { var record = entry.completion || {}; record.type = "normal", delete record.arg, entry.completion = record; } function Context(tryLocsList) { this.tryEntries = [{ tryLoc: "root" }], tryLocsList.forEach(pushTryEntry, this), this.reset(!0); } function values(iterable) { if (iterable) { var iteratorMethod = iterable[iteratorSymbol]; if (iteratorMethod) return iteratorMethod.call(iterable); if ("function" == typeof iterable.next) return iterable; if (!isNaN(iterable.length)) { var i = -1, next = function next() { for (; ++i < iterable.length;) if (hasOwn.call(iterable, i)) return next.value = iterable[i], next.done = !1, next; return next.value = undefined, next.done = !0, next; }; return next.next = next; } } return { next: doneResult }; } function doneResult() { return { value: undefined, done: !0 }; } return GeneratorFunction.prototype = GeneratorFunctionPrototype, defineProperty(Gp, "constructor", { value: GeneratorFunctionPrototype, configurable: !0 }), defineProperty(GeneratorFunctionPrototype, "constructor", { value: GeneratorFunction, configurable: !0 }), GeneratorFunction.displayName = define(GeneratorFunctionPrototype, toStringTagSymbol, "GeneratorFunction"), exports.isGeneratorFunction = function (genFun) { var ctor = "function" == typeof genFun && genFun.constructor; return !!ctor && (ctor === GeneratorFunction || "GeneratorFunction" === (ctor.displayName || ctor.name)); }, exports.mark = function (genFun) { return Object.setPrototypeOf ? Object.setPrototypeOf(genFun, GeneratorFunctionPrototype) : (genFun.__proto__ = GeneratorFunctionPrototype, define(genFun, toStringTagSymbol, "GeneratorFunction")), genFun.prototype = Object.create(Gp), genFun; }, exports.awrap = function (arg) { return { __await: arg }; }, defineIteratorMethods(AsyncIterator.prototype), define(AsyncIterator.prototype, asyncIteratorSymbol, function () { return this; }), exports.AsyncIterator = AsyncIterator, exports.async = function (innerFn, outerFn, self, tryLocsList, PromiseImpl) { void 0 === PromiseImpl && (PromiseImpl = Promise); var iter = new AsyncIterator(wrap(innerFn, outerFn, self, tryLocsList), PromiseImpl); return exports.isGeneratorFunction(outerFn) ? iter : iter.next().then(function (result) { return result.done ? result.value : iter.next(); }); }, defineIteratorMethods(Gp), define(Gp, toStringTagSymbol, "Generator"), define(Gp, iteratorSymbol, function () { return this; }), define(Gp, "toString", function () { return "[object Generator]"; }), exports.keys = function (val) { var object = Object(val), keys = []; for (var key in object) keys.push(key); return keys.reverse(), function next() { for (; keys.length;) { var key = keys.pop(); if (key in object) return next.value = key, next.done = !1, next; } return next.done = !0, next; }; }, exports.values = values, Context.prototype = { constructor: Context, reset: function reset(skipTempReset) { if (this.prev = 0, this.next = 0, this.sent = this._sent = undefined, this.done = !1, this.delegate = null, this.method = "next", this.arg = undefined, this.tryEntries.forEach(resetTryEntry), !skipTempReset) for (var name in this) "t" === name.charAt(0) && hasOwn.call(this, name) && !isNaN(+name.slice(1)) && (this[name] = undefined); }, stop: function stop() { this.done = !0; var rootRecord = this.tryEntries[0].completion; if ("throw" === rootRecord.type) throw rootRecord.arg; return this.rval; }, dispatchException: function dispatchException(exception) { if (this.done) throw exception; var context = this; function handle(loc, caught) { return record.type = "throw", record.arg = exception, context.next = loc, caught && (context.method = "next", context.arg = undefined), !!caught; } for (var i = this.tryEntries.length - 1; i >= 0; --i) { var entry = this.tryEntries[i], record = entry.completion; if ("root" === entry.tryLoc) return handle("end"); if (entry.tryLoc <= this.prev) { var hasCatch = hasOwn.call(entry, "catchLoc"), hasFinally = hasOwn.call(entry, "finallyLoc"); if (hasCatch && hasFinally) { if (this.prev < entry.catchLoc) return handle(entry.catchLoc, !0); if (this.prev < entry.finallyLoc) return handle(entry.finallyLoc); } else if (hasCatch) { if (this.prev < entry.catchLoc) return handle(entry.catchLoc, !0); } else { if (!hasFinally) throw new Error("try statement without catch or finally"); if (this.prev < entry.finallyLoc) return handle(entry.finallyLoc); } } } }, abrupt: function abrupt(type, arg) { for (var i = this.tryEntries.length - 1; i >= 0; --i) { var entry = this.tryEntries[i]; if (entry.tryLoc <= this.prev && hasOwn.call(entry, "finallyLoc") && this.prev < entry.finallyLoc) { var finallyEntry = entry; break; } } finallyEntry && ("break" === type || "continue" === type) && finallyEntry.tryLoc <= arg && arg <= finallyEntry.finallyLoc && (finallyEntry = null); var record = finallyEntry ? finallyEntry.completion : {}; return record.type = type, record.arg = arg, finallyEntry ? (this.method = "next", this.next = finallyEntry.finallyLoc, ContinueSentinel) : this.complete(record); }, complete: function complete(record, afterLoc) { if ("throw" === record.type) throw record.arg; return "break" === record.type || "continue" === record.type ? this.next = record.arg : "return" === record.type ? (this.rval = this.arg = record.arg, this.method = "return", this.next = "end") : "normal" === record.type && afterLoc && (this.next = afterLoc), ContinueSentinel; }, finish: function finish(finallyLoc) { for (var i = this.tryEntries.length - 1; i >= 0; --i) { var entry = this.tryEntries[i]; if (entry.finallyLoc === finallyLoc) return this.complete(entry.completion, entry.afterLoc), resetTryEntry(entry), ContinueSentinel; } }, "catch": function _catch(tryLoc) { for (var i = this.tryEntries.length - 1; i >= 0; --i) { var entry = this.tryEntries[i]; if (entry.tryLoc === tryLoc) { var record = entry.completion; if ("throw" === record.type) { var thrown = record.arg; resetTryEntry(entry); } return thrown; } } throw new Error("illegal catch attempt"); }, delegateYield: function delegateYield(iterable, resultName, nextLoc) { return this.delegate = { iterator: values(iterable), resultName: resultName, nextLoc: nextLoc }, "next" === this.method && (this.arg = undefined), ContinueSentinel; } }, exports; }
function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }
function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, _toPropertyKey(descriptor.key), descriptor); } }
function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }
function _defineProperty(obj, key, value) { key = _toPropertyKey(key); if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
function _toPropertyKey(arg) { var key = _toPrimitive(arg, "string"); return _typeof(key) === "symbol" ? key : String(key); }
function _toPrimitive(input, hint) { if (_typeof(input) !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (_typeof(res) !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }
/**
 * The engine module handles the simulation of rooms, the actions, the loading and persisting of every single thing happening. It is effectively what the server uses exclusively, + a mongoDB adapter for its data retrieval.
 */
var FloatingGardensEngine = /*#__PURE__*/function () {
  function FloatingGardensEngine(dataBackendAdapter) {
    _classCallCheck(this, FloatingGardensEngine);
    _defineProperty(this, "dataBackendAdapter", void 0);
    _defineProperty(this, "parties", {});
    _defineProperty(this, "users", {});
    _defineProperty(this, "rooms", {});
    _defineProperty(this, "lastUpdateTimestamp", Date.now());
    _defineProperty(this, "listeners", {});
    _defineProperty(this, "actionsPile", []);
    _defineProperty(this, "updateIntervalId", null);
    _defineProperty(this, "stopSignal", false);
    this.dataBackendAdapter = dataBackendAdapter;
  }
  _createClass(FloatingGardensEngine, [{
    key: "loadData",
    value: function () {
      var _loadData = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee() {
        var _yield$this$dataBacke, users, parties, rooms;
        return _regeneratorRuntime().wrap(function _callee$(_context) {
          while (1) switch (_context.prev = _context.next) {
            case 0:
              _context.next = 2;
              return this.dataBackendAdapter.loadData();
            case 2:
              _yield$this$dataBacke = _context.sent;
              users = _yield$this$dataBacke.users;
              parties = _yield$this$dataBacke.parties;
              rooms = _yield$this$dataBacke.rooms;
              this.parties = parties;
              this.users = users;
              this.rooms = rooms;
            case 9:
            case "end":
              return _context.stop();
          }
        }, _callee, this);
      }));
      function loadData() {
        return _loadData.apply(this, arguments);
      }
      return loadData;
    }()
  }, {
    key: "createNewUser",
    value: function () {
      var _createNewUser = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee2(_ref) {
        var login, password, pseudo, _yield$this$dataBacke2, user, party, room, inventory;
        return _regeneratorRuntime().wrap(function _callee2$(_context2) {
          while (1) switch (_context2.prev = _context2.next) {
            case 0:
              login = _ref.login, password = _ref.password, pseudo = _ref.pseudo;
              _context2.next = 3;
              return this.dataBackendAdapter.createNewUser({
                login: login,
                password: password,
                pseudo: pseudo
              });
            case 3:
              _yield$this$dataBacke2 = _context2.sent;
              user = _yield$this$dataBacke2.user;
              party = _yield$this$dataBacke2.party;
              room = _yield$this$dataBacke2.room;
              inventory = _yield$this$dataBacke2.inventory;
              this.users[String(user._id)] = user;
              this.parties[String(party._id)] = party;
              this.rooms[String(room._id)] = room;
              this.rooms[String(inventory._id)] = inventory;
              return _context2.abrupt("return", {
                user: user,
                party: party,
                room: room
              });
            case 13:
            case "end":
              return _context2.stop();
          }
        }, _callee2, this);
      }));
      function createNewUser(_x) {
        return _createNewUser.apply(this, arguments);
      }
      return createNewUser;
    }()
  }, {
    key: "createNewUserInParty",
    value: function () {
      var _createNewUserInParty = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee3(_ref2) {
        var login, password, pseudo, partyId;
        return _regeneratorRuntime().wrap(function _callee3$(_context3) {
          while (1) switch (_context3.prev = _context3.next) {
            case 0:
              login = _ref2.login, password = _ref2.password, pseudo = _ref2.pseudo, partyId = _ref2.partyId;
              _context3.next = 3;
              return this.dataBackendAdapter.createNewUser({
                login: login,
                password: password,
                pseudo: pseudo,
                partyId: partyId
              });
            case 3:
              return _context3.abrupt("return", _context3.sent);
            case 4:
            case "end":
              return _context3.stop();
          }
        }, _callee3, this);
      }));
      function createNewUserInParty(_x2) {
        return _createNewUserInParty.apply(this, arguments);
      }
      return createNewUserInParty;
    }()
  }, {
    key: "startUpdating",
    value: function startUpdating() {
      this.lastUpdateTimestamp = Date.now();
      // Stupidly simple and naïve implementation
      this.updateIntervalId = setInterval(this.update.bind(this), 500);
      console.log("Starting the update loop...");
    }
  }, {
    key: "stop",
    value: function stop() {
      this.stopSignal = true;
    }
  }, {
    key: "update",
    value: function () {
      var _update = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee4() {
        var now;
        return _regeneratorRuntime().wrap(function _callee4$(_context4) {
          while (1) switch (_context4.prev = _context4.next) {
            case 0:
              now = Date.now();
              this.handleUserActions(now);
              this.handleActions(now);
              this.updateRooms(now);

              // No more interval, means we've been stopped. Persist the data.
              if (!this.stopSignal) {
                _context4.next = 11;
                break;
              }
              console.log("Stopping the update loop and saving...");
              clearInterval(this.updateIntervalId);
              this.updateIntervalId = null;
              _context4.next = 10;
              return this.dataBackendAdapter.saveAll({
                users: this.users,
                parties: this.parties,
                rooms: this.rooms
              });
            case 10:
              this.callEventListener(_events.EVENTS.STOPPED_SERVER);
            case 11:
            case "end":
              return _context4.stop();
          }
        }, _callee4, this);
      }));
      function update() {
        return _update.apply(this, arguments);
      }
      return update;
    }()
  }, {
    key: "addActionToHandle",
    value: function addActionToHandle(_ref3) {
      var actionId = _ref3.actionId,
        context = _ref3.context,
        data = _ref3.data,
        cb = _ref3.cb;
      this.actionsPile.push({
        actionId: actionId,
        context: context,
        data: data,
        cb: cb
      });
    }
  }, {
    key: "handleUserActions",
    value: function handleUserActions(now) {
      var _this = this;
      var _loop = function _loop() {
        var user = _Object$values[_i];
        if (!user.currentAction) return "continue";
        if (user.currentAction.completionDate < now) {
          var room = _this.rooms[user.currentAction.roomId];
          var element = room.elements.find(function (e) {
            return e.id === user.currentAction.elementId;
          });
          if (!element) {
            user.currentAction = null;
            _this.callEventListener(_events.EVENTS.UPDATED_USER, (0, _actions.cleanUser)(user));
            return "continue";
          }
          var action = (0, _element_helpers.stage)(element).actions.find(function (a) {
            return a.id === user.currentAction.actionId;
          });
          if (!action) {
            user.currentAction = null;
            _this.callEventListener(_events.EVENTS.UPDATED_USER, (0, _actions.cleanUser)(user));
            return "continue";
          }
          action.apply({
            engine: _this,
            user: user,
            room: room,
            element: element
          });
          user.currentAction = null;
          _this.callEventListener(_events.EVENTS.UPDATED_USER, (0, _actions.cleanUser)(user));
        }
      };
      for (var _i = 0, _Object$values = Object.values(this.users); _i < _Object$values.length; _i++) {
        var _ret = _loop();
        if (_ret === "continue") continue;
      }
    }
  }, {
    key: "handleActions",
    value: function handleActions() {
      var actionsCount = this.actionsPile.length;
      for (var i = 0; i < actionsCount; i++) {
        var action = this.actionsPile.shift();
        this.handleAction(action);
      }
    }
  }, {
    key: "handleAction",
    value: function handleAction(action) {
      var actionId = action.actionId,
        context = action.context,
        data = action.data,
        cb = action.cb;
      if (!_actions.ACTION_HANDLERS[actionId]) {
        console.warn("Unknown action ID \"".concat(actionId, "\". Something's wrong."));
        return;
      }
      _actions.ACTION_HANDLERS[actionId](_objectSpread({
        engine: this
      }, context), data, cb);
    }
  }, {
    key: "updateRooms",
    value: function updateRooms(now) {
      var delta = (now - this.lastUpdateTimestamp) / 1000;
      for (var _i2 = 0, _Object$values2 = Object.values(this.rooms); _i2 < _Object$values2.length; _i2++) {
        var room = _Object$values2[_i2];
        room.contextProperties = (0, _element_helpers.computeContextProperties)({
          room: room
        });
        var biome = _biomes.BIOMES[room.biome];
        if (biome) {
          biome.update({
            engine: this,
            room: room,
            delta: delta
          });
        }
        var _iterator = _createForOfIteratorHelper(room.elements),
          _step;
        try {
          for (_iterator.s(); !(_step = _iterator.n()).done;) {
            var element = _step.value;
            this.updateRoomElement({
              room: room,
              delta: delta
            }, element);
          }
        } catch (err) {
          _iterator.e(err);
        } finally {
          _iterator.f();
        }
        this.callEventListener(_events.EVENTS.UPDATED_ROOM, room);
      }
      this.lastUpdateTimestamp = Date.now();
      this.callEventListener(_events.EVENTS.POST_UPDATE);
    }
  }, {
    key: "updateRoomElement",
    value: function updateRoomElement(ctx, element) {
      var elementTemplate = (0, _element_helpers.template)(element);
      var stage = elementTemplate.stages[element.stage];
      stage.update(ctx, element);
      if (!stage.transitions) return;
      var _iterator2 = _createForOfIteratorHelper(stage.transitions),
        _step2;
      try {
        for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
          var transition = _step2.value;
          if (transition.condition(ctx, element)) {
            element.stage = transition.to;
          }
        }
      } catch (err) {
        _iterator2.e(err);
      } finally {
        _iterator2.f();
      }
    }
  }, {
    key: "callEventListener",
    value: function callEventListener(eventId, data) {
      if (!this.listeners[eventId]) return;else this.listeners[eventId](data);
    }
  }, {
    key: "setEventListener",
    value: function setEventListener(eventId, cb) {
      if (!_events.EVENTS[eventId]) throw new Error("Event ID \"".concat(eventId, "\" doesn't exist"));
      this.listeners[eventId] = cb;
    }
  }]);
  return FloatingGardensEngine;
}();
exports.FloatingGardensEngine = FloatingGardensEngine;