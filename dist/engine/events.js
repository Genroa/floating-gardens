"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.EVENTS = void 0;
var EVENTS = {
  // Server finished stopping its update loop and saving the current state of the game
  STOPPED_SERVER: "STOPPED_SERVER",
  // Server updated a room
  UPDATED_ROOM: "UPDATED_ROOM",
  // Server updated the state of a user
  UPDATED_USER: "UPDATED_USER",
  USER_MOVED: "USER_MOVED",
  // Server updated the state of a party
  UPDATED_PARTY: "UPDATED_PARTY",
  // Server finished one update cycle
  POST_UPDATE: "POST_UPDATE"
};
exports.EVENTS = EVENTS;