"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.cleanUser = exports.ACTION_HANDLERS = exports.ACTIONS = void 0;
var _element_helpers = require("../helpers/element_helpers");
var _events = require("./events");
var ACTIONS = {
  // Query the list of all users of the querier's party
  QUERY_PARTY_USERS: "QUERY_PARTY_USERS",
  // Query the list of all rooms of the querier's party
  QUERY_PARTY_ROOMS: "QUERY_PARTY_ROOMS",
  // Change the pseudo of the querier
  CHANGE_USER_PSEUDO: "CHANGE_USER_PSEUDO",
  MOVE_TO_ROOM: "MOVE_TO_ROOM",
  START_ELEMENT_ACTION: "START_ELEMENT_ACTION"
};
exports.ACTIONS = ACTIONS;
var cleanUser = function cleanUser(u) {
  return {
    _id: u._id,
    pseudo: u.pseudo,
    party: u.party,
    visitingRoom: u.visitingRoom,
    lastActive: u.lastActive,
    currentAction: u.currentAction
  };
};

// These are what the engine does when it processes these actions.
exports.cleanUser = cleanUser;
var ACTION_HANDLERS = {
  QUERY_PARTY_USERS: function QUERY_PARTY_USERS(_ref, _, cb) {
    var engine = _ref.engine,
      user = _ref.user;
    var users = Object.values(engine.users).filter(function (u) {
      return u.party === user.party;
    });
    cb === null || cb === void 0 ? void 0 : cb(users.map(cleanUser));
  },
  QUERY_PARTY_ROOMS: function QUERY_PARTY_ROOMS(_ref2, _, cb) {
    var engine = _ref2.engine,
      party = _ref2.party;
    var rooms = Object.values(engine.rooms).filter(function (i) {
      return party.rooms.includes(String(i._id)) || party.inventory === String(i._id);
    });
    cb === null || cb === void 0 ? void 0 : cb(rooms);
  },
  CHANGE_USER_PSEUDO: function CHANGE_USER_PSEUDO(_ref3, pseudo, cb) {
    var engine = _ref3.engine,
      user = _ref3.user;
    user.pseudo = pseudo.substring(0, 20);
    cb === null || cb === void 0 ? void 0 : cb(user);
    engine.callEventListener(_events.EVENTS.UPDATED_USER, cleanUser(user));
  },
  MOVE_TO_ROOM: function MOVE_TO_ROOM(_ref4, roomId, cb) {
    var engine = _ref4.engine,
      user = _ref4.user,
      party = _ref4.party;
    if (!party.rooms.includes(roomId)) {
      console.log("Tried to move to a room this party doesn't know about.");
      return cb === null || cb === void 0 ? void 0 : cb(user);
    }
    var previousRoomId = user.visitingRoom;
    user.visitingRoom = roomId;
    cb === null || cb === void 0 ? void 0 : cb(user);
    engine.callEventListener(_events.EVENTS.USER_MOVED, {
      previousRoomId: previousRoomId,
      user: cleanUser(user)
    });
  },
  START_ELEMENT_ACTION: function START_ELEMENT_ACTION(_ref5, _ref6, cb) {
    var engine = _ref5.engine,
      user = _ref5.user,
      party = _ref5.party;
    var roomId = _ref6.roomId,
      elementId = _ref6.elementId,
      actionId = _ref6.actionId;
    var currentAction = user.currentAction;
    if (currentAction) {
      console.log("Tried to do an action but the player is already doing something.");
      return cb === null || cb === void 0 ? void 0 : cb(null);
    }
    var room = engine.rooms[roomId];
    // Room not found
    if ((!party.rooms.includes(roomId) || !room) && party.inventory !== roomId) {
      console.log("Tried to do an action on a room the player cannot access (or the room referenced doesn't exist).");
      return cb === null || cb === void 0 ? void 0 : cb(null);
    }
    var element = room.elements.find(function (e) {
      return e.id === elementId;
    });
    // Element not found
    if (!element) {
      console.log("Tried to do an action on an element that doesn't exist.");
      return cb === null || cb === void 0 ? void 0 : cb(null);
    }
    var action = (0, _element_helpers.stage)(element).actions.find(function (a) {
      return a.id === actionId;
    });
    // Conditions aren't met
    if (action.condition && !action.condition({
      engine: engine,
      room: room,
      element: element,
      user: user,
      party: party
    })) {
      console.log("Tried to do an action but conditions weren't met.");
      return cb === null || cb === void 0 ? void 0 : cb(null);
    }

    // Wrong context
    if (!action.available({
      user: user,
      room: room
    })) {
      console.log("Tried to do an action in the wrong context.");
      return cb === null || cb === void 0 ? void 0 : cb(null);
    }
    user.currentAction = {
      roomId: roomId,
      elementId: elementId,
      actionId: actionId,
      completionDate: Date.now() + action.warmup * 1000
    };
    cb === null || cb === void 0 ? void 0 : cb();
    engine.callEventListener(_events.EVENTS.UPDATED_USER, cleanUser(user));
  }
};
exports.ACTION_HANDLERS = ACTION_HANDLERS;