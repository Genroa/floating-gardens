"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.computeContextProperties = computeContextProperties;
exports.computeRandomGeneValues = computeRandomGeneValues;
exports.deleteElement = void 0;
exports.description = description;
exports.get = void 0;
exports.label = label;
exports.newElementFromTemplate = newElementFromTemplate;
exports.percentChanceDelta = percentChanceDelta;
exports.randomInRange = randomInRange;
exports.stage = stage;
exports.template = template;
exports.transitionBelowThreshold = exports.transitionAboveThreshold = void 0;
var _element_templates = require("../element_templates");
var _lodash = require("lodash");
var _uuid = require("uuid");
function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }
function _createForOfIteratorHelper(o, allowArrayLike) { var it = typeof Symbol !== "undefined" && o[Symbol.iterator] || o["@@iterator"]; if (!it) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e2) { throw _e2; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = it.call(o); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e3) { didErr = true; err = _e3; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }
function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }
function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }
function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i]; return arr2; }
function _iterableToArrayLimit(arr, i) { var _i = null == arr ? null : "undefined" != typeof Symbol && arr[Symbol.iterator] || arr["@@iterator"]; if (null != _i) { var _s, _e, _x, _r, _arr = [], _n = !0, _d = !1; try { if (_x = (_i = _i.call(arr)).next, 0 === i) { if (Object(_i) !== _i) return; _n = !1; } else for (; !(_n = (_s = _x.call(_i)).done) && (_arr.push(_s.value), _arr.length !== i); _n = !0); } catch (err) { _d = !0, _e = err; } finally { try { if (!_n && null != _i["return"] && (_r = _i["return"](), Object(_r) !== _r)) return; } finally { if (_d) throw _e; } } return _arr; } }
function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }
function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }
function _defineProperty(obj, key, value) { key = _toPropertyKey(key); if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
function _toPropertyKey(arg) { var key = _toPrimitive(arg, "string"); return _typeof(key) === "symbol" ? key : String(key); }
function _toPrimitive(input, hint) { if (_typeof(input) !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (_typeof(res) !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }
var get = function get(state, value) {
  var _state$value;
  return (_state$value = state[value]) !== null && _state$value !== void 0 ? _state$value : 0;
};
exports.get = get;
var transitionBelowThreshold = function transitionBelowThreshold(stage, internalProperty, thresholdValue) {
  return {
    to: stage,
    condition: function condition(ctx, _ref) {
      var state = _ref.state;
      return get(state, internalProperty) <= thresholdValue;
    }
  };
};
exports.transitionBelowThreshold = transitionBelowThreshold;
var transitionAboveThreshold = function transitionAboveThreshold(stage, internalProperty, thresholdValue) {
  return {
    to: stage,
    condition: function condition(ctx, _ref2) {
      var state = _ref2.state;
      return get(state, internalProperty) > thresholdValue;
    }
  };
};
exports.transitionAboveThreshold = transitionAboveThreshold;
var deleteElement = function deleteElement(room, id) {
  return room.elements = room.elements.filter(function (e) {
    return e.id !== id;
  });
};
exports.deleteElement = deleteElement;
function stage(element) {
  return template(element).stages[element.stage];
}
function template(element) {
  return _element_templates.ELEMENT_TEMPLATES[element.type];
}
function label(room, element) {
  var _stage$label;
  var func = (_stage$label = stage(element).label) !== null && _stage$label !== void 0 ? _stage$label : template(element).label;
  if (typeof func === "string") return func;
  if (!func) return element.type;
  return func({
    room: room
  }, element);
}
function description(room, element) {
  var _stage$description;
  var func = (_stage$description = stage(element).description) !== null && _stage$description !== void 0 ? _stage$description : template(element).description;
  if (typeof func === "string") return func;
  if (!func) return element.type;
  return func({
    room: room
  }, element);
}
function randomInRange(min, max) {
  return Math.random() * (max - min) + min;
}
function percentChanceDelta(delta, chance) {
  return randomInRange(0, 100) >= 100 - chance * delta;
}
function newElementFromTemplate(templateId) {
  var _template$startingSta, _template$startingSta2;
  var template = _element_templates.ELEMENT_TEMPLATES[templateId];
  return {
    id: (0, _uuid.v4)(),
    genes: computeRandomGeneValues(template.genes),
    type: templateId,
    stage: template.startingStage,
    state: _objectSpread({}, (_template$startingSta = (_template$startingSta2 = template.startingState) === null || _template$startingSta2 === void 0 ? void 0 : _template$startingSta2.call(template)) !== null && _template$startingSta !== void 0 ? _template$startingSta : {})
  };
}
function computeRandomGeneValues(genes) {
  if (!genes) return {};
  return Object.fromEntries(Object.entries(genes !== null && genes !== void 0 ? genes : {}).map(function (_ref3) {
    var _ref4 = _slicedToArray(_ref3, 2),
      key = _ref4[0],
      infos = _ref4[1];
    return [key, randomInRange(infos.min, infos.max)];
  }));
}
function computeContextProperties(ctx) {
  var contextProperties = {};
  var _iterator = _createForOfIteratorHelper(ctx.room.elements),
    _step;
  try {
    for (_iterator.s(); !(_step = _iterator.n()).done;) {
      var _stage$properties, _stage$properties2, _stage;
      var element = _step.value;
      var properties = (_stage$properties = (_stage$properties2 = (_stage = stage(element)).properties) === null || _stage$properties2 === void 0 ? void 0 : _stage$properties2.call(_stage, ctx, element)) !== null && _stage$properties !== void 0 ? _stage$properties : {};
      (0, _lodash.mergeWith)(contextProperties, properties, function (objValue, srcValue) {
        return (objValue !== null && objValue !== void 0 ? objValue : 0) + (srcValue !== null && srcValue !== void 0 ? srcValue : 0);
      });
    }
    // if (isPresent.value)
    //   mergeWith(
    //     contextProperties,
    //     { presence: 1 },
    //     (objValue, srcValue) => (objValue ?? 0) + (srcValue ?? 0)
    //   );
  } catch (err) {
    _iterator.e(err);
  } finally {
    _iterator.f();
  }
  return contextProperties;
}