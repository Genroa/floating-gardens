"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }
Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "ACTIONS", {
  enumerable: true,
  get: function get() {
    return _actions.ACTIONS;
  }
});
Object.defineProperty(exports, "ACTION_HANDLERS", {
  enumerable: true,
  get: function get() {
    return _actions.ACTION_HANDLERS;
  }
});
Object.defineProperty(exports, "BIOMES", {
  enumerable: true,
  get: function get() {
    return _index2.BIOMES;
  }
});
Object.defineProperty(exports, "ELEMENT_TEMPLATES", {
  enumerable: true,
  get: function get() {
    return _index.ELEMENT_TEMPLATES;
  }
});
Object.defineProperty(exports, "EVENTS", {
  enumerable: true,
  get: function get() {
    return _events.EVENTS;
  }
});
Object.defineProperty(exports, "FloatingGardensEngine", {
  enumerable: true,
  get: function get() {
    return _index3.FloatingGardensEngine;
  }
});
Object.defineProperty(exports, "cleanUser", {
  enumerable: true,
  get: function get() {
    return _actions.cleanUser;
  }
});
exports.elementHelpers = void 0;
var _index = require("./element_templates/index");
var _index2 = require("./biomes/index");
var _events = require("./engine/events");
var _actions = require("./engine/actions");
var _index3 = require("./engine/index");
var _elementHelpers = _interopRequireWildcard(require("./helpers/element_helpers"));
exports.elementHelpers = _elementHelpers;
function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }
function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }