"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.BIOMES = void 0;
var _element_helpers = require("../helpers/element_helpers");
var _room_helpers = require("../helpers/room_helpers");
var BIOMES = {
  VINE_RUIN: {
    update: function update(_ref) {
      var delta = _ref.delta,
        engine = _ref.engine,
        room = _ref.room;
      var availableSize = (0, _room_helpers.computeAvailableSize)(room);
      if (availableSize > 0) {
        // console.log(
        //   room.name,
        //   "=> Space left to populate with wild scarecrows:",
        //   availableSize
        // );
        for (var i = 0; i < availableSize; i++) {
          if ((0, _element_helpers.percentChanceDelta)(delta, 0.2)) {
            var newElement = (0, _element_helpers.newElementFromTemplate)("RUIN_WEED");
            room.elements.push(newElement);
            // console.log("Spawned new element!");
          }
        }
      } else {
        // console.log(room.name, " => No space left.");
      }
    }
  }
};
exports.BIOMES = BIOMES;