"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SHOWOFF_WHEAT = void 0;
var _element_helpers = require("../helpers/element_helpers");
var SPROUT_THRESHOLD = 20;
var ADULT_THRESHOLD = 40;
var SHOWOFF_WHEAT = {
  genes: {
    GROWTH_FACTOR: {
      min: 0.5,
      max: 2,
      mutationFactor: 0.2
    }
  },
  description: "This strange wheat only grows if it thinks someone's around.",
  startingStage: "SEED",
  stages: {
    SEED: {
      size: 1,
      actions: [],
      label: "Showoff Wheat (seed)",
      update: function update(_ref, _ref2) {
        var delta = _ref.delta;
        var state = _ref2.state,
          genes = _ref2.genes;
        state.growth = (0, _element_helpers.get)(state, "growth") + 1 * delta * genes.GROWTH_FACTOR;
      },
      transitions: [(0, _element_helpers.transitionAboveThreshold)("SPROUT", "growth", SPROUT_THRESHOLD)]
    },
    SPROUT: {
      size: 1,
      actions: [],
      label: "Showoff Wheat 🌱 (sprout)",
      update: function update(_ref3, _ref4) {
        var delta = _ref3.delta,
          room = _ref3.room;
        var state = _ref4.state,
          genes = _ref4.genes;
        if ((0, _element_helpers.get)(room.contextProperties, "presence") === 0) return;
        state.growth = (0, _element_helpers.get)(state, "growth") + 1 * delta * genes.GROWTH_FACTOR;
      },
      transitions: [(0, _element_helpers.transitionBelowThreshold)("SEED", "growth", SPROUT_THRESHOLD), (0, _element_helpers.transitionAboveThreshold)("ADULT", "growth", ADULT_THRESHOLD)]
    },
    ADULT: {
      size: 1,
      actions: [],
      label: "Showoff Wheat 🌿 (adult)",
      update: function update(ctx, element) {
        // if(computeContextValue(ctx, "presence") === 0) return;
        // state.growth = get(state, "growth") + (1 * genes.GROWTH_FACTOR)
      },
      transitions: [(0, _element_helpers.transitionBelowThreshold)("SPROUT", "growth", ADULT_THRESHOLD)]
    }
  }
};
exports.SHOWOFF_WHEAT = SHOWOFF_WHEAT;