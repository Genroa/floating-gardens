"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.RUIN_WEED = void 0;
var _element_helpers = require("../helpers/element_helpers");
var SPROUT_THRESHOLD = 20;
var MATURE_THRESHOLD = 40;
var removeAction = {
  id: "remove",
  label: "Arracher",
  warmup: 2,
  available: function available(_ref) {
    var user = _ref.user,
      room = _ref.room;
    return true;
  },
  description: "Arracher cette plante. Ne fournit aucune récompense.",
  apply: function apply(_ref2) {
    var room = _ref2.room,
      element = _ref2.element;
    room.elements = room.elements.filter(function (e) {
      return e.id !== element.id;
    });
  }
};
var RUIN_WEED = {
  genes: {
    GROWTH_FACTOR: {
      min: 0.5,
      max: 2,
      mutationFactor: 0.2
    },
    ACIDITY_ABSORPTION: {
      min: 0.01,
      max: 0.05
    }
  },
  startingStage: "SEED",
  description: "Cette variété de plante semble être une des rares espèces poussant à l'état naturel au sein des pauvres îlots de caillasse et de ruines comme celui sur lequel vous avez démarré votre histoire. Elle semble particulièrement apte à s'adapter à différents sols hostiles à la vie.",
  stages: {
    SEED: {
      size: 1,
      actions: [],
      label: "Herbes de ruines (graine)",
      update: function update(_ref3, _ref4) {
        var delta = _ref3.delta;
        var state = _ref4.state,
          genes = _ref4.genes;
        state.growth = (0, _element_helpers.get)(state, "growth") + 1 * delta * genes.GROWTH_FACTOR;
      },
      transitions: [(0, _element_helpers.transitionAboveThreshold)("SPROUT", "growth", SPROUT_THRESHOLD)]
    },
    SPROUT: {
      size: 1,
      actions: [removeAction],
      label: "Herbes de ruines 🌱 (immature)",
      update: function update(_ref5, _ref6) {
        var delta = _ref5.delta,
          room = _ref5.room;
        var state = _ref6.state,
          genes = _ref6.genes;
        var acidity = (0, _element_helpers.get)(room.contextProperties, "acidity");
        state.growth = Math.max(0, (0, _element_helpers.get)(state, "growth") + 1 * delta * genes.GROWTH_FACTOR - acidity * 0.05);
      },
      transitions: [(0, _element_helpers.transitionAboveThreshold)("MATURE", "growth", MATURE_THRESHOLD)]
    },
    MATURE: {
      size: 1,
      actions: [removeAction],
      label: "Herbes de ruines 🌿 (mature)",
      update: function update(_ref7, _ref8) {
        var delta = _ref7.delta,
          room = _ref7.room;
        var state = _ref8.state,
          genes = _ref8.genes;
        var acidity = (0, _element_helpers.get)(room.contextProperties, "acidity");
        state.growth = Math.min(Math.max(0, (0, _element_helpers.get)(state, "growth") + 1 * delta * genes.GROWTH_FACTOR - acidity * 0.08), 100);
      },
      transitions: [(0, _element_helpers.transitionBelowThreshold)("SPROUT", "growth", MATURE_THRESHOLD - 10)]
    }
  }
};
exports.RUIN_WEED = RUIN_WEED;