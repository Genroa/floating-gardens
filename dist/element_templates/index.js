"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ELEMENT_TEMPLATES = void 0;
var _scarecrow = require("./scarecrow");
var _showoff_wheat = require("./showoff_wheat");
var _ruine_weed = require("./ruine_weed");
var ELEMENT_TEMPLATES = {
  SCARECROW: _scarecrow.SCARECROW,
  SHOWOFF_WHEAT: _showoff_wheat.SHOWOFF_WHEAT,
  RUIN_WEED: _ruine_weed.RUIN_WEED
};
exports.ELEMENT_TEMPLATES = ELEMENT_TEMPLATES;