"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SCARECROW = void 0;
var _element_helpers = require("../helpers/element_helpers");
var SCARECROW = {
  label: "A Quirky Scarecrow",
  description: "A weird human-looking puppet.",
  startingState: function startingState() {
    return {
      durability: 300
    };
  },
  startingStage: "BASE",
  stages: {
    BASE: {
      size: 2,
      update: function update(_ref, _ref2) {
        var delta = _ref.delta;
        var state = _ref2.state;
        state.durability = (0, _element_helpers.get)(state, "durability") - 1 * delta;
      },
      properties: function properties() {
        return {
          presence: 1
        };
      },
      transitions: [(0, _element_helpers.transitionBelowThreshold)("BROKEN", "durability", 100)]
    },
    BROKEN: {
      size: 2,
      label: "Broken Scarecrow",
      description: "A weird human-looking puppet. Currently in a pretty sorry state of disrepair.",
      update: function update(_ref3, _ref4) {
        var room = _ref3.room,
          delta = _ref3.delta;
        var state = _ref4.state,
          id = _ref4.id;
        state.durability = (0, _element_helpers.get)(state, "durability") - 1 * delta;
        if (state.durability <= 0) (0, _element_helpers.deleteElement)(room, id);
      }
    }
  }
};
exports.SCARECROW = SCARECROW;