import {
  newElementFromTemplate,
  percentChanceDelta,
} from "../helpers/element_helpers";
import { computeAvailableSize } from "../helpers/room_helpers";

export const BIOMES = {
  VINE_RUIN: {
    update({ delta, engine, room }) {
      const availableSize = computeAvailableSize(room);
      if (availableSize > 0) {
        // console.log(
        //   room.name,
        //   "=> Space left to populate with wild scarecrows:",
        //   availableSize
        // );
        for (let i = 0; i < availableSize; i++) {
          if (percentChanceDelta(delta, 0.2)) {
            const newElement = newElementFromTemplate("RUIN_WEED");
            room.elements.push(newElement);
            // console.log("Spawned new element!");
          }
        }
      } else {
        // console.log(room.name, " => No space left.");
      }
    },
  },
};
