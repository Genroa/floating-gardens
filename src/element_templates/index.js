import { SCARECROW } from "./scarecrow";
import { SHOWOFF_WHEAT } from "./showoff_wheat";
import { RUIN_WEED } from "./ruine_weed";

export const ELEMENT_TEMPLATES = {
  SCARECROW,
  SHOWOFF_WHEAT,
  RUIN_WEED,
};
