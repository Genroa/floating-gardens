import {
  get,
  transitionAboveThreshold,
  transitionBelowThreshold,
} from "../helpers/element_helpers";

const SPROUT_THRESHOLD = 20;
const ADULT_THRESHOLD = 40;

export const SHOWOFF_WHEAT = {
  genes: {
    GROWTH_FACTOR: { min: 0.5, max: 2, mutationFactor: 0.2 },
  },
  description: "This strange wheat only grows if it thinks someone's around.",
  startingStage: "SEED",
  stages: {
    SEED: {
      size: 1,
      actions: [],
      label: "Showoff Wheat (seed)",
      update({ delta }, { state, genes }) {
        state.growth = get(state, "growth") + 1 * delta * genes.GROWTH_FACTOR;
      },

      transitions: [
        transitionAboveThreshold("SPROUT", "growth", SPROUT_THRESHOLD),
      ],
    },
    SPROUT: {
      size: 1,
      actions: [],
      label: "Showoff Wheat 🌱 (sprout)",
      update({ delta, room }, { state, genes }) {
        if (get(room.contextProperties, "presence") === 0) return;
        state.growth = get(state, "growth") + 1 * delta * genes.GROWTH_FACTOR;
      },

      transitions: [
        transitionBelowThreshold("SEED", "growth", SPROUT_THRESHOLD),
        transitionAboveThreshold("ADULT", "growth", ADULT_THRESHOLD),
      ],
    },
    ADULT: {
      size: 1,
      actions: [],
      label: "Showoff Wheat 🌿 (adult)",
      update(ctx, element) {
        // if(computeContextValue(ctx, "presence") === 0) return;
        // state.growth = get(state, "growth") + (1 * genes.GROWTH_FACTOR)
      },

      transitions: [
        transitionBelowThreshold("SPROUT", "growth", ADULT_THRESHOLD),
      ],
    },
  },
};
