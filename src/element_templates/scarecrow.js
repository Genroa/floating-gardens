import {
  get,
  deleteElement,
  transitionBelowThreshold,
} from "../helpers/element_helpers";

export const SCARECROW = {
  label: "A Quirky Scarecrow",
  description: "A weird human-looking puppet.",
  startingState: () => ({ durability: 300 }),
  startingStage: "BASE",
  stages: {
    BASE: {
      size: 2,
      update({ delta }, { state }) {
        state.durability = get(state, "durability") - 1 * delta;
      },
      properties: () => ({ presence: 1 }),
      transitions: [transitionBelowThreshold("BROKEN", "durability", 100)],
    },
    BROKEN: {
      size: 2,
      label: "Broken Scarecrow",
      description:
        "A weird human-looking puppet. Currently in a pretty sorry state of disrepair.",
      update({ room, delta }, { state, id }) {
        state.durability = get(state, "durability") - 1 * delta;
        if (state.durability <= 0) deleteElement(room, id);
      },
    },
  },
};
