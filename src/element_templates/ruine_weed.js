import {
  get,
  transitionAboveThreshold,
  transitionBelowThreshold,
} from "../helpers/element_helpers";

const SPROUT_THRESHOLD = 20;
const MATURE_THRESHOLD = 40;

const removeAction = {
  id: "remove",
  label: "Arracher",
  warmup: 2,
  available: ({ user, room }) => true,
  description: "Arracher cette plante. Ne fournit aucune récompense.",
  apply({ room, element }) {
    room.elements = room.elements.filter((e) => e.id !== element.id);
  },
};

export const RUIN_WEED = {
  genes: {
    GROWTH_FACTOR: { min: 0.5, max: 2, mutationFactor: 0.2 },
    ACIDITY_ABSORPTION: { min: 0.01, max: 0.05 },
  },
  startingStage: "SEED",
  description:
    "Cette variété de plante semble être une des rares espèces poussant à l'état naturel au sein des pauvres îlots de caillasse et de ruines comme celui sur lequel vous avez démarré votre histoire. Elle semble particulièrement apte à s'adapter à différents sols hostiles à la vie.",
  stages: {
    SEED: {
      size: 1,
      actions: [],
      label: "Herbes de ruines (graine)",
      update({ delta }, { state, genes }) {
        state.growth = get(state, "growth") + 1 * delta * genes.GROWTH_FACTOR;
      },
      transitions: [
        transitionAboveThreshold("SPROUT", "growth", SPROUT_THRESHOLD),
      ],
    },
    SPROUT: {
      size: 1,
      actions: [removeAction],
      label: "Herbes de ruines 🌱 (immature)",
      update({ delta, room }, { state, genes }) {
        const acidity = get(room.contextProperties, "acidity");
        state.growth = Math.max(
          0,
          get(state, "growth") +
            1 * delta * genes.GROWTH_FACTOR -
            acidity * 0.05
        );
      },

      transitions: [
        transitionAboveThreshold("MATURE", "growth", MATURE_THRESHOLD),
      ],
    },
    MATURE: {
      size: 1,
      actions: [removeAction],
      label: "Herbes de ruines 🌿 (mature)",
      update({ delta, room }, { state, genes }) {
        const acidity = get(room.contextProperties, "acidity");
        state.growth = Math.min(
          Math.max(
            0,
            get(state, "growth") +
              1 * delta * genes.GROWTH_FACTOR -
              acidity * 0.08
          ),
          100
        );
      },

      transitions: [
        transitionBelowThreshold("SPROUT", "growth", MATURE_THRESHOLD - 10),
      ],
    },
  },
};
