import { stage } from "./element_helpers";

export function computeAvailableSize(room) {
  let space = room.size;
  for (const element of room.elements) {
    space -= stage(element).size;
  }
  return space;
}
