import { ELEMENT_TEMPLATES } from "../element_templates";
import { mergeWith } from "lodash";
import { v4 as uuid } from "uuid";

export const get = (state, value) => state[value] ?? 0;

export const transitionBelowThreshold = (
  stage,
  internalProperty,
  thresholdValue
) => ({
  to: stage,
  condition(ctx, { state }) {
    return get(state, internalProperty) <= thresholdValue;
  },
});

export const transitionAboveThreshold = (
  stage,
  internalProperty,
  thresholdValue
) => ({
  to: stage,
  condition(ctx, { state }) {
    return get(state, internalProperty) > thresholdValue;
  },
});

export const deleteElement = (room, id) =>
  (room.elements = room.elements.filter((e) => e.id !== id));

export function stage(element) {
  return template(element).stages[element.stage];
}

export function template(element) {
  return ELEMENT_TEMPLATES[element.type];
}

export function label(room, element) {
  let func = stage(element).label ?? template(element).label;
  if (typeof func === "string") return func;
  if (!func) return element.type;
  return func({ room }, element);
}

export function description(room, element) {
  let func = stage(element).description ?? template(element).description;
  if (typeof func === "string") return func;
  if (!func) return element.type;
  return func({ room }, element);
}

export function randomInRange(min, max) {
  return Math.random() * (max - min) + min;
}

export function percentChanceDelta(delta, chance) {
  return randomInRange(0, 100) >= 100 - chance * delta;
}

export function newElementFromTemplate(templateId) {
  let template = ELEMENT_TEMPLATES[templateId];
  return {
    id: uuid(),
    genes: computeRandomGeneValues(template.genes),
    type: templateId,
    stage: template.startingStage,
    state: { ...(template.startingState?.() ?? {}) },
  };
}

export function computeRandomGeneValues(genes) {
  if (!genes) return {};
  return Object.fromEntries(
    Object.entries(genes ?? {}).map(([key, infos]) => {
      return [key, randomInRange(infos.min, infos.max)];
    })
  );
}

export function computeContextProperties(ctx) {
  let contextProperties = {};
  for (const element of ctx.room.elements) {
    const properties = stage(element).properties?.(ctx, element) ?? {};

    mergeWith(
      contextProperties,
      properties,
      (objValue, srcValue) => (objValue ?? 0) + (srcValue ?? 0)
    );
  }
  // if (isPresent.value)
  //   mergeWith(
  //     contextProperties,
  //     { presence: 1 },
  //     (objValue, srcValue) => (objValue ?? 0) + (srcValue ?? 0)
  //   );
  return contextProperties;
}
