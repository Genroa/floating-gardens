import { stage } from "../helpers/element_helpers";
import { EVENTS } from "./events";

export const ACTIONS = {
  // Query the list of all users of the querier's party
  QUERY_PARTY_USERS: "QUERY_PARTY_USERS",
  // Query the list of all rooms of the querier's party
  QUERY_PARTY_ROOMS: "QUERY_PARTY_ROOMS",
  // Change the pseudo of the querier
  CHANGE_USER_PSEUDO: "CHANGE_USER_PSEUDO",

  MOVE_TO_ROOM: "MOVE_TO_ROOM",
  START_ELEMENT_ACTION: "START_ELEMENT_ACTION",
};

export const cleanUser = (u) => ({
  _id: u._id,
  pseudo: u.pseudo,
  party: u.party,
  visitingRoom: u.visitingRoom,
  lastActive: u.lastActive,
  currentAction: u.currentAction,
});

// These are what the engine does when it processes these actions.
export const ACTION_HANDLERS = {
  QUERY_PARTY_USERS: ({ engine, user }, _, cb) => {
    const users = Object.values(engine.users).filter(
      (u) => u.party === user.party
    );
    cb?.(users.map(cleanUser));
  },
  QUERY_PARTY_ROOMS: ({ engine, party }, _, cb) => {
    const rooms = Object.values(engine.rooms).filter(
      (i) =>
        party.rooms.includes(String(i._id)) || party.inventory === String(i._id)
    );
    cb?.(rooms);
  },
  CHANGE_USER_PSEUDO: ({ engine, user }, pseudo, cb) => {
    user.pseudo = pseudo.substring(0, 20);
    cb?.(user);
    engine.callEventListener(EVENTS.UPDATED_USER, cleanUser(user));
  },
  MOVE_TO_ROOM: ({ engine, user, party }, roomId, cb) => {
    if (!party.rooms.includes(roomId)) {
      console.log("Tried to move to a room this party doesn't know about.");
      return cb?.(user);
    }
    const previousRoomId = user.visitingRoom;
    user.visitingRoom = roomId;
    cb?.(user);
    engine.callEventListener(EVENTS.USER_MOVED, {
      previousRoomId,
      user: cleanUser(user),
    });
  },

  START_ELEMENT_ACTION: (
    { engine, user, party },
    { roomId, elementId, actionId },
    cb
  ) => {
    const currentAction = user.currentAction;
    if (currentAction) {
      console.log(
        "Tried to do an action but the player is already doing something."
      );
      return cb?.(null);
    }

    const room = engine.rooms[roomId];
    const inventory = engine.rooms[party.inventory];
    // Room not found
    if (
      (!party.rooms.includes(roomId) || !room) &&
      party.inventory !== roomId
    ) {
      console.log(
        "Tried to do an action on a room the player cannot access (or the room referenced doesn't exist)."
      );
      return cb?.(null);
    }

    const element = room.elements.find((e) => e.id === elementId);
    // Element not found
    if (!element) {
      console.log("Tried to do an action on an element that doesn't exist.");
      return cb?.(null);
    }

    const action = stage(element).actions.find((a) => a.id === actionId);
    // Conditions aren't met
    if (
      action.condition &&
      !action.condition({ engine, room, element, user, party })
    ) {
      console.log("Tried to do an action but conditions weren't met.");
      return cb?.(null);
    }

    // Wrong context
    if (!action.available({ user, room, inventory })) {
      console.log("Tried to do an action in the wrong context.");
      return cb?.(null);
    }

    user.currentAction = {
      roomId,
      elementId,
      actionId,
      completionDate: Date.now() + action.warmup * 1000,
    };
    cb?.();
    engine.callEventListener(EVENTS.UPDATED_USER, cleanUser(user));
  },
};
