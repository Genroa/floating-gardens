import {
  computeContextProperties,
  stage,
  template,
} from "../helpers/element_helpers";
import { EVENTS } from "./events";
import { ACTION_HANDLERS, cleanUser } from "./actions";
import { BIOMES } from "../biomes";

/**
 * The engine module handles the simulation of rooms, the actions, the loading and persisting of every single thing happening. It is effectively what the server uses exclusively, + a mongoDB adapter for its data retrieval.
 */

export class FloatingGardensEngine {
  dataBackendAdapter;
  parties = {};
  users = {};
  rooms = {};
  lastUpdateTimestamp = Date.now();
  listeners = {};
  actionsPile = [];
  updateIntervalId = null;
  stopSignal = false;

  constructor(dataBackendAdapter) {
    this.dataBackendAdapter = dataBackendAdapter;
  }

  async loadData() {
    const { users, parties, rooms } = await this.dataBackendAdapter.loadData();
    this.parties = parties;
    this.users = users;
    this.rooms = rooms;
  }

  async createNewUser({ login, password, pseudo }) {
    const { user, party, room, inventory } =
      await this.dataBackendAdapter.createNewUser({
        login,
        password,
        pseudo,
      });
    this.users[String(user._id)] = user;
    this.parties[String(party._id)] = party;
    this.rooms[String(room._id)] = room;
    this.rooms[String(inventory._id)] = inventory;
    return { user, party, room };
  }

  async createNewUserInParty({ login, password, pseudo, partyId }) {
    return await this.dataBackendAdapter.createNewUser({
      login,
      password,
      pseudo,
      partyId,
    });
  }

  startUpdating() {
    this.lastUpdateTimestamp = Date.now();
    // Stupidly simple and naïve implementation
    this.updateIntervalId = setInterval(this.update.bind(this), 500);
    console.log("Starting the update loop...");
  }

  stop() {
    this.stopSignal = true;
  }

  async update() {
    const now = Date.now();
    this.handleUserActions(now);
    this.handleActions(now);
    this.updateRooms(now);

    // No more interval, means we've been stopped. Persist the data.
    if (this.stopSignal) {
      console.log("Stopping the update loop and saving...");
      clearInterval(this.updateIntervalId);
      this.updateIntervalId = null;
      await this.dataBackendAdapter.saveAll({
        users: this.users,
        parties: this.parties,
        rooms: this.rooms,
      });
      this.callEventListener(EVENTS.STOPPED_SERVER);
    }
  }

  addActionToHandle({ actionId, context, data, cb }) {
    this.actionsPile.push({ actionId, context, data, cb });
  }

  handleUserActions(now) {
    for (const user of Object.values(this.users)) {
      if (!user.currentAction) continue;
      if (user.currentAction.completionDate >= now) continue;

      const party = this.parties[user.party];
      const inventory = this.rooms[party.inventory];
      const room = this.rooms[user.currentAction.roomId];
      const element = room.elements.find(
        (e) => e.id === user.currentAction.elementId
      );
      if (!element) {
        user.currentAction = null;
        this.callEventListener(EVENTS.UPDATED_USER, cleanUser(user));
        continue;
      }
      const action = stage(element).actions.find(
        (a) => a.id === user.currentAction.actionId
      );

      if (!action) {
        user.currentAction = null;
        this.callEventListener(EVENTS.UPDATED_USER, cleanUser(user));
        continue;
      }

      action.apply({ engine: this, user, room, element, inventory });

      user.currentAction = null;
      this.callEventListener(EVENTS.UPDATED_USER, cleanUser(user));
    }
  }

  handleActions() {
    const actionsCount = this.actionsPile.length;
    for (let i = 0; i < actionsCount; i++) {
      const action = this.actionsPile.shift();
      this.handleAction(action);
    }
  }

  handleAction(action) {
    const { actionId, context, data, cb } = action;
    if (!ACTION_HANDLERS[actionId]) {
      console.warn(`Unknown action ID "${actionId}". Something's wrong.`);
      return;
    }
    ACTION_HANDLERS[actionId]({ engine: this, ...context }, data, cb);
  }

  updateRooms(now) {
    const delta = (now - this.lastUpdateTimestamp) / 1000;
    for (const room of Object.values(this.rooms)) {
      room.contextProperties = computeContextProperties({ room });

      const biome = BIOMES[room.biome];
      if (biome) {
        biome.update({ engine: this, room, delta });
      }

      for (const element of room.elements)
        this.updateRoomElement({ engine: this, room, delta }, element);

      this.callEventListener(EVENTS.UPDATED_ROOM, room);
    }
    this.lastUpdateTimestamp = Date.now();
    this.callEventListener(EVENTS.POST_UPDATE);
  }

  updateRoomElement(ctx, element) {
    const elementTemplate = template(element);
    const stage = elementTemplate.stages[element.stage];
    stage.update(ctx, element);

    if (!stage.transitions) return;
    for (const transition of stage.transitions) {
      if (transition.condition(ctx, element)) {
        element.stage = transition.to;
      }
    }
  }

  callEventListener(eventId, data) {
    if (!this.listeners[eventId]) return;
    else this.listeners[eventId](data);
  }

  setEventListener(eventId, cb) {
    if (!EVENTS[eventId])
      throw new Error(`Event ID "${eventId}" doesn't exist`);
    this.listeners[eventId] = cb;
  }
}
